package base;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseTest {

	private static WebDriver driver = null;

	public static WebDriver getDriver() {
		return driver;
	}

	public BaseTest() {
		if (driver == null) {
			System.setProperty("webdriver.chrome.driver",
					"src\\test\\java\\resources\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().deleteAllCookies();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		}
	}

}
