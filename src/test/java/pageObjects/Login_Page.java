package pageObjects;

import java.util.Hashtable;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Login_Page extends BasePage{

	public Login_Page(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}


	@FindBy(xpath = "//span[contains(text(),'Hello. Sign in')]")
	public static WebElement helloSignIn;
	
//	@FindBy(xpath = "//nav[@id='nav-link-accountList']")
//	public static WebElement helloSignIn;

	@FindBy(xpath = "//input[@id='ap_email']")
	public static WebElement emailMobNum;

	@FindBy(css = "#continue")
	public static WebElement btn_Continue;

	@FindBy(xpath = "//input[@id='ap_password']")
	public static WebElement password;

	@FindBy(xpath = "//input[@id='signInSubmit']")
	public static WebElement Login;
	
	
	public void login(Hashtable<String, String> map)
	{
		helloSignIn.click();
		//Adding wait time for sign in page to load - Fix for "unable to loacate xpath for emailMobNum"
		Login_Page.getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		emailMobNum.sendKeys(map.get("username"));
		btn_Continue.click();
		password.sendKeys(map.get("pasword"));
		Login.click();
		
	}
	

}
