package pageObjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

import base.BaseTest;

public class ToolsQAForm extends BasePage {

	private static int TIME = 2000;
	JavascriptExecutor js = (JavascriptExecutor) BaseTest.getDriver();
	public ToolsQAForm(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	@FindBy(xpath="//a[@id='cookie_action_close_header']")
	public static WebElement bCookieAccept;
	
	
	@FindBy(xpath = "//input[contains(@name,'firstname')]")
	public static WebElement sFirstName;

	
	@FindBy(xpath = "//input[@id='lastname']")
	public static WebElement sLastName;
	
	@FindBy(xpath = "//button[@id='buttonwithclass']")
	public static WebElement sButton;

	@FindBy(xpath = "//em[contains(text(),'ToolsQA is a leading provider of technology conten')]")
	public static WebElement stext;
	
//	@FindAll(value = @FindBy(name="sex"))
//	public static List<WebElement> sexRadioButton;
	
//	@FindBy(how = How.NAME, using = "sex")
//	public static List<WebElement> sexRadioButtn;
	
	
	List<WebElement> sexRadioButton =  BaseTest.getDriver().findElements(By.name("sex"));
	
	@FindBy(how = How.ID, using = "exp-2")
	private static WebElement exp;
	
	
	List<WebElement> profession = BaseTest.getDriver().findElements(By.name("profession"));
	
	@FindBy(xpath="//input[@id='photo']")
	private static WebElement photo;
	
	@FindBy(xpath = "//a[contains(text(),'Test File to Download')]")
	private static WebElement dlink;
	
	List<WebElement> tools = BaseTest.getDriver().findElements(By.name("tool"));
	
	WebElement continents = BaseTest.getDriver().findElement(By.id("continents"));
	
	@FindBy(xpath="//select[@id='continentsmultiple']")
	private static WebElement conm;
	
	@FindBy(xpath="//select[@id='selenium_commands']")
	private static WebElement scmds;
	

	public void submit() throws InterruptedException {
		//org.openqa.selenium.ElementClickInterceptedException:
		//Element is not coming in view, hence not getting clicked
//		Thread.sleep(TIME);
//		js.executeScript("arguments[0].scrollIntoView();", sButton);
//		        sButton.click();
//		        Thread.sleep(TIME);
//				if(stext.isDisplayed())
//					System.out.println(stext.getText());
				
	}
	
	
	public void fillForm(Hashtable<String, String> map) throws InterruptedException {
		
//		System.out.println("Printing map value");
//		System.out.println(map.get("FirstName"));
		if(map!=null) {
		if(bCookieAccept.isDisplayed())
			bCookieAccept.click();
		Thread.sleep(TIME);
		sFirstName.sendKeys(map.get("FirstName"));
		Thread.sleep(TIME);
		sLastName.sendKeys(map.get("LastName"));
		Thread.sleep(TIME);
		
		for(WebElement rButton : sexRadioButton) {
			if(!rButton.isSelected()) {
				rButton.click();
			}
		}
		Thread.sleep(TIME);
		exp.click();
		Thread.sleep(TIME);
		for(int i=0; i< profession.size(); i++) {
			String val = profession.get(i).getAttribute("value");
			if(val.equalsIgnoreCase("Automation Tester"))
			{
				profession.get(i).click();
				break;
			}
		}
		Thread.sleep(TIME);		
		//photo.click();
		js.executeScript("arguments[0].scrollIntoView();", photo);
		
		//Uplaoding a file
		photo.sendKeys("C:\\Users\\Laveena_PC\\Documents\\BE-fees_receipt_exam.pdf");
		Thread.sleep(TIME);
		
		//Downloading a file
		dlink.click();
		
		for(int j=0;j<tools.size();j++)
		{
			if(tools.get(j).getAttribute("value").equalsIgnoreCase("Selenium Webdriver")) {
				tools.get(j).click();
				break;
			}
				
		}
		Select obj1 = new Select(continents);
		obj1.selectByValue("NA");
		Thread.sleep(TIME);
		
		Select obj = new Select(conm);
		if(obj.isMultiple()) {
			obj.deselectAll();
			Thread.sleep(TIME);
			obj.selectByIndex(0);
			Thread.sleep(TIME);
			obj.deselectByIndex(0);
			Thread.sleep(TIME);
			
			List<WebElement> options = obj.getOptions();
			Thread.sleep(TIME);
			for(int k=0;k<options.size();k++) {
				obj.selectByIndex(k);
			}
		}
		
		Select obj2 = new Select(scmds);
		obj2.deselectAll();
		obj2.selectByVisibleText("WebElement Commands");
		Thread.sleep(TIME);
		
	}
}
}
