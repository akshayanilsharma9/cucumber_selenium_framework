package stepDefinitions;

import base.BaseTest;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.ToolsQAForm;

public class ToolsQAForm_Definition extends BaseTest {
	
	public ToolsQAForm formObj = new ToolsQAForm(BaseTest.getDriver());
	
	@When("User enters Firstname and Lastname")
	public void user_enters_Firstname_and_Lastname() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new cucumber.api.PendingException();
		formObj.fillForm(map);
	}

	@When("User clicks on submit")
	public void user_clicks_on_submit() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new cucumber.api.PendingException();
		formObj.submit();
	}

	@Then("application submit should be successful")
	public void application_submit_should_be_successful() {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new cucumber.api.PendingException();
	}



}
