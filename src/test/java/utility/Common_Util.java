package utility;

import java.util.Hashtable;

public class Common_Util {

	public static String path = System.getProperty("user.dir");
	
	public static XLS_Reader XL_Initilization(String xls_name)
	{
		try{
			
			XLS_Reader xls = new XLS_Reader(path+"//src//test//java//resources//"+xls_name+".xlsx");
			return xls;
		}
		catch(Exception e){
			return null;
		}
		
	}
	
	public static Hashtable<String, String> funcGetData(String xls_Name,String TCID_Name,String sheet_Name)
	{
		
		try{
			Hashtable<String,String> map = new Hashtable<>();
			XLS_Reader xls = XL_Initilization(xls_Name);
			int colCount = xls.getColumnCount(sheet_Name);
			int TCIDRowNum = xls.getCellRowNum(sheet_Name, "TCID", TCID_Name);
			
			Object[][] data = new Object[2][colCount];
			for(int i = 1;i <= colCount;i++)
			{
				String auto_ID = xls.getCellData(sheet_Name, i, 1);
				String test_Data = xls.getCellData(sheet_Name, i, TCIDRowNum);
				
				data[0][i-1] = auto_ID;
				data[1][i-1] = test_Data;
				
				
			}
			
			for(int i = 0;i < colCount;i++)
			{
				String key = data[0][i].toString().trim();
				String value = data[1][i].toString().trim();
				map.put(key, value);
				
			}
			xls = null;
			return map;
			
		}catch(Exception e)
		{
			
			System.out.println(e);
			
		}
		
		
		
		return null;
		
	}
	

	
	
	
}

