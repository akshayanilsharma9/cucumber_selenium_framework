package utility;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Calendar;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.sl.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import com.sun.xml.internal.ws.api.streaming.XMLStreamReaderFactory.Woodstox;

public class XLS_Reader {

	public String path = "";
	public FileInputStream fin = null;
	public FileOutputStream fout = null;
	public static Workbook workbook = null;
	public static org.apache.poi.ss.usermodel.Sheet sheet = null;
	public static Row row = null;
	public Cell cell = null;
	public String ext = null;
	public CreationHelper createHelper = null;
	
	
	
	public XLS_Reader(String path) {
	
		this.path = path;
		System.out.println(this.path);
		try{
			fin = new FileInputStream(path);
			ext = path.substring(path.indexOf(".xlsx"));
			if(ext.equals(".xlsx"))
				workbook = new XSSFWorkbook(fin);
			else if(ext.equals(".xls"))
				workbook = new HSSFWorkbook(fin);
				
			
		fin.close();	
		}catch(Exception e)
		{
			
			
		}
	}
	
	public int getCellRowNum(String sheetName,String colName,String cellValue)
	{
		
		
		for(int i = 2;i <= getRowCount(sheetName);i++)
		{
			
			if(getCellData(sheetName,colName,i).equalsIgnoreCase(cellValue))
			{
				return i;
			}
			
		}
		return -1;
	}

	public int getRowCount(String sheetName) {
		int index = workbook.getSheetIndex(sheetName);
		if(index == -1)
			return 0;
		else{
			sheet = workbook.getSheetAt(index);
			int number = sheet.getLastRowNum();
			return number;
		}
	}

	public String getCellData(String sheetName, String colName, int Row_Num) {
		// TODO Auto-generated method stub
		
		if(Row_Num <= 0)
		{
			return "";
		}
		
		
	
		
		int index = workbook.getSheetIndex(sheetName);
		int col_Num = -1;
		if(index == -1)
			return "";
		
		sheet = workbook.getSheetAt(index);
		row = sheet.getRow(0);
		
		for(int i = 0;i<row.getLastCellNum();i++)
		{
			if(row.getCell(i).getStringCellValue().trim().equals(colName.trim()))
				col_Num = i;
		}
		if(col_Num==-1)
			return "";
		
		sheet = workbook.getSheetAt(index);
		row = sheet.getRow(Row_Num-1);
		if(row == null)
			return "";
		cell = row.getCell(col_Num);
		if(cell == null)
			return "";

		if(cell.getCellType() == CellType.STRING)
		{
			
			return cell.getStringCellValue();
		}
		
		else if(cell.getCellType() == CellType.NUMERIC || cell.getCellType() == CellType.FORMULA){
				
				String cellText = String.valueOf(cell.getNumericCellValue());
		
				if(HSSFDateUtil.isCellDateFormatted(cell)){
					
					double d = cell.getNumericCellValue();
					
					Calendar cal = Calendar.getInstance();
					
					cal.setTime(HSSFDateUtil.getJavaDate(d));
					cellText = String.valueOf(cal.get(Calendar.YEAR)).substring(0);
					cellText = (cal.get(Calendar.MONTH)+1)+"/"+cal.get(Calendar.DAY_OF_MONTH)+"/"+cellText;
					
				}
				return cellText;
		}
		return "";
		
		
	
	}

	
	

	public String getCellData(String sheetName, int Col_Num, int Row_Num) {
		// TODO Auto-generated method stub
		
		if(Row_Num <= 0)
		{
			return "Row num < 0";
		}
		
		int index = workbook.getSheetIndex(sheetName);
		if(index == -1)
			return "";
		
		sheet = workbook.getSheetAt(index);
		row = sheet.getRow(Row_Num-1);
		if(row == null)
			return "";
		cell = row.getCell(Col_Num);
		
		if(cell == null)
			return "";
		
		
		if(cell.getCellType() == CellType.STRING)
		{
			
			return cell.getStringCellValue();
		}
		
		else if(cell.getCellType() == CellType.NUMERIC || cell.getCellType() == CellType.FORMULA){
				
				String cellText = String.valueOf(cell.getNumericCellValue());
		
				if(HSSFDateUtil.isCellDateFormatted(cell)){
					
					double d = cell.getNumericCellValue();
					
					Calendar cal = Calendar.getInstance();
					
					cal.setTime(HSSFDateUtil.getJavaDate(d));
					cellText = String.valueOf(cal.get(Calendar.YEAR)).substring(0);
					cellText = (cal.get(Calendar.MONTH)+1)+"/"+cal.get(Calendar.DAY_OF_MONTH)+"/"+cellText;
					
				}
				return cellText;
		}
		return "";
		
		
	
	}

	

	
	
	public static int getColumnCount(String sheetName) {
		
		if(!isSheetExist(sheetName))
			return -1;
		
		sheet = workbook.getSheet(sheetName);
		row = sheet.getRow(0);
		
		if(row == null)
		{
			return -1;
		}
		
		// TODO Auto-generated method stub
		return row.getLastCellNum();
	}

	public static boolean isSheetExist(String sheetName) {
		
		int index = workbook.getSheetIndex(sheetName);
		if(index == -1)
		{
			index = workbook.getSheetIndex(sheetName.toUpperCase());
			if (index == -1)
				return false;
			else
				return true;
		
		}
		else	
			return true;
	}
}
