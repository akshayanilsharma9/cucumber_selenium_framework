$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/java/features/endToend.feature");
formatter.feature({
  "name": "EndToEnd Test I want to Test my application",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@ApplicationTest"
    }
  ]
});
formatter.scenarioOutline({
  "name": "User Login to application",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@logincheck"
    }
  ]
});
formatter.step({
  "name": "Initializing data from excel sheet \u0027\u003csheetname\u003e\u0027 for test case \u0027\u003cTCID\u003e\u0027 from document \u0027\u003cdocumentname\u003e\u0027",
  "keyword": "Given "
});
formatter.step({
  "name": "User enters username and password",
  "keyword": "When "
});
formatter.step({
  "name": "application login should be successful",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "TCID",
        "sheetname",
        "documentname"
      ]
    },
    {
      "cells": [
        "TC_01",
        "UserData",
        "TestData"
      ]
    }
  ]
});
formatter.scenario({
  "name": "User Login to application",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ApplicationTest"
    },
    {
      "name": "@logincheck"
    }
  ]
});
formatter.step({
  "name": "Initializing data from excel sheet \u0027UserData\u0027 for test case \u0027TC_01\u0027 from document \u0027TestData\u0027",
  "keyword": "Given "
});
formatter.match({
  "location": "Login_Definition.initializing_data_from_excel_sheet_for_test_case_from_document(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters username and password",
  "keyword": "When "
});
formatter.match({
  "location": "Login_Definition.user_enters_username_and_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "application login should be successful",
  "keyword": "Then "
});
formatter.match({
  "location": "Login_Definition.application_login_should_be_successful()"
});
formatter.result({
  "status": "passed"
});
});